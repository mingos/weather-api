var gulp = require("gulp");
var ts = require("gulp-typescript");
var merge = require("merge-stream");

gulp.task("ts:src", function() {
	return gulp.src("src/**/*.ts")
		.pipe(ts({
			module: "commonjs",
			target: "es5",
			noImplicitAny: true
		}))
		.pipe(gulp.dest("src"));
});

gulp.task("ts:spec", function() {
	return gulp.src("spec/**/*.ts")
		.pipe(ts({
			module: "commonjs",
			target: "es5",
			noImplicitAny: true
		}))
		.pipe(gulp.dest("spec"));
});

gulp.task("ts:dev", ["ts:src", "ts:spec"]);

gulp.task("ts:dist", function() {
	var result = gulp.src(["src/index.ts", "src/**/*.ts"])
		.pipe(ts({
			module: "commonjs",
			target: "es5",
			declaration: true,
			noImplicitAny: true
		}));

	var stream1 = result.dts
		.pipe(gulp.dest("lib"));
	var stream2 = result.js
		.pipe(gulp.dest("lib"));

	return merge(stream1, stream2);
});
