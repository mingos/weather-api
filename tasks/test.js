var gulp = require("gulp");
var jasmine = require("gulp-jasmine");
var SpecReporter = require("jasmine-spec-reporter");

gulp.task("test", function() {
	gulp.src("spec/**/*.spec.js")
		.pipe(jasmine({
			reporter: new SpecReporter({
				colors: true,
				displayStackTrace: true
			})
		}));
});
