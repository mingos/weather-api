var gulp = require("gulp");

gulp.task("watch", function() {
	gulp.watch("src/**/*.ts", ["ts:src"]);
	gulp.watch("spec/**/*.ts", ["ts:spec"]);
});
