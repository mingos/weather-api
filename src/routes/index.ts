import * as express from "express";
import {WeatherFetcher} from "../services/WeatherFetcher";
import {Http} from "../services/Http";
import {WeatherFetcherProvider} from "../services/WeatherFetcherProvider";
import * as YAML from "yamljs";
import {WeatherOptions} from "../services/WeatherOptions";

export let indexRouter: express.Router = express.Router();

let weatherFetcherProvider = new WeatherFetcherProvider(YAML.load("config/config.yml"));
let weatherFetcher: WeatherFetcher = weatherFetcherProvider.get();

indexRouter.get("/weather", (request: express.Request, response: express.Response) => {
	let options: WeatherOptions = request.query;
	weatherFetcher.getWeather(options).then((res) => {
		response.send(res);
	});
});

