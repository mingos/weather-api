import * as request from "request";

export class Http {
	public get(options: request.RequiredUriUrl, callback: request.RequestCallback): void {
		request.get(options, callback);
	}
}
