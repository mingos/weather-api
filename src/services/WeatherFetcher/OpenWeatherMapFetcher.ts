import {WeatherFetcher} from "../WeatherFetcher";
import {WeatherOptions} from "../WeatherOptions";
import {Weather} from "../Weather";
import * as extend from "extend";
import {Http} from "../Http";

export class OpenWeatherMapFetcher implements WeatherFetcher {
	private apiUrl: string;
	private qs: {};
	private http: Http;

	public constructor(apiKey: string, http: Http) {
		this.qs = {
			appid: apiKey,
			units: "metric"
		};
		this.apiUrl = "http://api.openweathermap.org/data/2.5/weather";
		this.http = http;
	}

	public getWeather(options: WeatherOptions): Promise<Weather> {
		let requestOptions = {
			uri: this.apiUrl,
			qs: extend({q: [options.city, options.country].join(",")}, this.qs)
		};

		return new Promise((fulfill, reject) => {
			this.http.get(requestOptions, (error, response, body) => {
				if (error) {
					reject(error);
				} else {
					let responseBody = JSON.parse(body);

					fulfill({
						temperature: responseBody.main.temp
					});
				}
			});
		});
	}
}
