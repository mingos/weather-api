import {WeatherFetcher} from "../WeatherFetcher";
import {WeatherOptions} from "../WeatherOptions";
import {Weather} from "../Weather";
import * as extend from "extend";
import {Http} from "../Http";

export class ForecastIoFetcher implements WeatherFetcher {
	private apiUrl: string;
	private qs: {};
	private http: Http;

	public constructor(apiKey: string, http: Http) {
		this.apiUrl = `https://api.forecast.io/forecast/${apiKey}`;
		this.qs = {
			units: "si"
		};
		this.http = http;
	}

	public getWeather(options: WeatherOptions): Promise<Weather> {
		let latitude = encodeURIComponent(options.latitude.toString());
		let longitude = encodeURIComponent(options.longitude.toString());
		let requestOptions = {
			uri: `${this.apiUrl}/${latitude},${longitude}`,
			qs: this.qs
		};

		return new Promise((fulfill, reject) => {
			this.http.get(requestOptions, (error, response, body) => {
				if (error) {
					reject(error);
				} else {
					let responseBody = JSON.parse(body);

					fulfill({
						temperature: responseBody.currently.temperature
					});
				}
			});
		});
	}
}
