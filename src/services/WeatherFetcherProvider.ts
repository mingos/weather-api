import {WeatherFetcher} from "./WeatherFetcher";
import {WeatherConfig} from "./WeatherConfig";
import {Http} from "./Http";
import {WeatherFetcherConstructor} from "./WeatherFetcherConstructor";

export class WeatherFetcherProvider {
	private config: WeatherConfig;

	public constructor(config: WeatherConfig) {
		this.config = config;
	}

	public get(): WeatherFetcher {
		let fetcher: WeatherFetcherConstructor =
			require(`./WeatherFetcher/${this.config.fetcher.implementation}`)[this.config.fetcher.implementation];
		return new fetcher(this.config.fetcher.apiKey, new Http());
	}
}
