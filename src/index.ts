/// <reference path="../typings/tsd.d.ts"/>

import * as express from "express";
import {cors} from "./middleware/cors";
import {indexRouter} from "./routes/index";
import * as path from "path";

let port: number = process.env.PORT || 3000;
let app: express.Express = express();

// serve static files
app.use(express.static(path.join(__dirname, "../public")));

// custom middlewares
app.use(cors);

// routes
app.use("/", indexRouter);

// start server
app.listen(port, () => {
	console.log(`Magic happens on port ${port}`);
});
