import {Request, Response} from "express";

let corsMiddleware = (request: Request, response: Response, next: Function): void => {
	response.setHeader("Access-Control-Allow-Origin", "*");

	if ("OPTIONS" === request.method) {
		response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,POST,PUT,DELETE");
	}

	next();
};

export var cors = corsMiddleware;
