import {OpenWeatherMapFetcher} from "../../../src/services/WeatherFetcher/OpenWeatherMapFetcher";

describe("Open Weather Map weather fetcher", () => {
	let weatherFetcher: OpenWeatherMapFetcher;
	let http: any;

	beforeEach(() => {
		http = {
			get: (options: any, callback: Function) => {
				callback(null, {}, JSON.stringify(require("./OpenWeatherMapResponse.json")));
			}
		};
		weatherFetcher = new OpenWeatherMapFetcher("foo", http);
	});

	it("fetches weather for a city", (done) => {
		// given
		// when
		let promise = weatherFetcher.getWeather({
			city: "Gdańsk",
			country: "Poland",
			countryCode: "PL",
			latitude: 54.3610063,
			longitude: 18.5484488
		});

		// then
		promise.then((result) => {
			expect(result).toEqual({temperature: 3.11});
			done();
		});
	});
});
