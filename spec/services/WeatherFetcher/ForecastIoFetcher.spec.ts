import {ForecastIoFetcher} from "../../../src/services/WeatherFetcher/ForecastIoFetcher";

describe("Forecast.io weather fetcher", () => {
	let weatherFetcher: ForecastIoFetcher;
	let http: any;

	beforeEach(() => {
		http = {
			get: (options: any, callback: Function) => {
				callback(null, {}, JSON.stringify(require("./ForecastIoResponse.json")));
			}
		};
		weatherFetcher = new ForecastIoFetcher("foo", http);
	});

	it("fetches weather for a city", (done) => {
		// given
		// when
		let promise = weatherFetcher.getWeather({
			city: "Gdańsk",
			country: "Poland",
			countryCode: "PL",
			latitude: 54.3610063,
			longitude: 18.5484488
		});

		// then
		promise.then((result) => {
			expect(result).toEqual({temperature: -0.25});
			done();
		});
	});
});
