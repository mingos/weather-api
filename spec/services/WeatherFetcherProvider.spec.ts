import {WeatherFetcherProvider} from "../../src/services/WeatherFetcherProvider";
import {OpenWeatherMapFetcher} from "../../src/services/WeatherFetcher/OpenWeatherMapFetcher";
import {ForecastIoFetcher} from "../../src/services/WeatherFetcher/ForecastIoFetcher";

describe("WeatherFetcherProvider", () => {
	it("can spawn an OpenWeatherMapFetcher", () => {
		// given
		let config = {
			fetcher: {
				implementation: "OpenWeatherMapFetcher",
				apiKey: "foo"
			}
		};
		let provider = new WeatherFetcherProvider(config);

		// when
		let fetcher = provider.get();

		// then
		expect(fetcher instanceof OpenWeatherMapFetcher).toBe(true);
	});

	it("can spawn a ForecastIoFetcher", () => {
		// given
		let config = {
			fetcher: {
				implementation: "ForecastIoFetcher",
				apiKey: "bar"
			}
		};
		let provider = new WeatherFetcherProvider(config);

		// when
		let fetcher = provider.get();

		// then
		expect(fetcher instanceof ForecastIoFetcher).toBe(true);
	});
});
