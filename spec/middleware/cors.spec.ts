/// <reference path="../../typings/tsd.d.ts"/>

import {cors} from "../../src/middleware/cors";

describe("cors middleware", () => {
	let corsMiddleware: Function = cors;
	let request: any;
	let response: any;
	let next: any;

	beforeEach(() => {
		request = {
			method: "GET"
		};
		response = {
			setHeader: (name: string, value: string): void => {}
		};
		next = {
			next: () => {}
		};
	});

	it("adds origin header", () => {
		// given
		spyOn(response, "setHeader");
		spyOn(next, "next");

		// when
		corsMiddleware(request, response, next.next);

		// then
		expect(response.setHeader).toHaveBeenCalledWith("Access-Control-Allow-Origin", "*");
		expect(next.next).toHaveBeenCalled();
	});

	it("adds methods header on OPTIONS", () => {
		// given
		spyOn(response, "setHeader");
		spyOn(next, "next");

		request.method = "OPTIONS";

		// when
		corsMiddleware(request, response, next.next);

		// then
		expect(response.setHeader).toHaveBeenCalledWith("Access-Control-Allow-Methods", "GET,HEAD,POST,PUT,DELETE");
		expect(next.next).toHaveBeenCalled();
	});
});
