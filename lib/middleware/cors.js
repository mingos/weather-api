var corsMiddleware = function (request, response, next) {
    response.setHeader("Access-Control-Allow-Origin", "*");
    if ("OPTIONS" === request.method) {
        response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,POST,PUT,DELETE");
    }
    next();
};
exports.cors = corsMiddleware;
