var express = require("express");
var WeatherFetcherProvider_1 = require("../services/WeatherFetcherProvider");
var YAML = require("yamljs");
exports.indexRouter = express.Router();
var weatherFetcherProvider = new WeatherFetcherProvider_1.WeatherFetcherProvider(YAML.load("config/config.yml"));
var weatherFetcher = weatherFetcherProvider.get();
exports.indexRouter.get("/weather", function (request, response) {
    var options = request.query;
    weatherFetcher.getWeather(options).then(function (res) {
        response.send(res);
    });
});
