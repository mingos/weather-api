/// <reference path="../typings/tsd.d.ts"/>
var express = require("express");
var cors_1 = require("./middleware/cors");
var index_1 = require("./routes/index");
var path = require("path");
var port = process.env.PORT || 3000;
var app = express();
// serve static files
app.use(express.static(path.join(__dirname, "../public")));
// custom middlewares
app.use(cors_1.cors);
// routes
app.use("/", index_1.indexRouter);
// start server
app.listen(port, function () {
    console.log("Magic happens on port " + port);
});
