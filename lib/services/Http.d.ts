import * as request from "request";
export declare class Http {
    get(options: request.RequiredUriUrl, callback: request.RequestCallback): void;
}
