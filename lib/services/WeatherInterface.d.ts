export interface WeatherInterface {
    city: string;
    country: string;
    temperature: number;
}
