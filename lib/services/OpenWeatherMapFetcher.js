var http = require("http");
var OpenWeatherMapFetcher = (function () {
    function OpenWeatherMapFetcher(apiKey) {
        this.apiUrl = "http://api.openweathermap.org/data/2.5/weather?appid=" + apiKey + "&units=metric&q=";
    }
    OpenWeatherMapFetcher.prototype.getWeather = function (city, country) {
        var _this = this;
        return new Promise(function (fulfill, reject) {
            http.get(_this.apiUrl + [city, country].join(","), function (response) {
                fulfill(response.headers);
            });
        });
    };
    return OpenWeatherMapFetcher;
})();
exports.OpenWeatherMapFetcher = OpenWeatherMapFetcher;
