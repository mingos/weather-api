import { WeatherFetcherInterface } from "./WeatherFetcherInterface";
import { WeatherInterface } from "./WeatherInterface";
export declare class OpenWeatherMapFetcher implements WeatherFetcherInterface {
    private apiUrl;
    constructor(apiKey: string);
    getWeather(city: string, country?: string): Promise<WeatherInterface>;
}
