var request = require("request");
var Http = (function () {
    function Http() {
    }
    Http.prototype.get = function (options, callback) {
        request.get(options, callback);
    };
    return Http;
})();
exports.Http = Http;
