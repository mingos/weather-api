var Http_1 = require("./Http");
var WeatherFetcherProvider = (function () {
    function WeatherFetcherProvider(config) {
        this.config = config;
    }
    WeatherFetcherProvider.prototype.get = function () {
        var fetcher = require("./WeatherFetcher/" + this.config.fetcher.implementation)[this.config.fetcher.implementation];
        return new fetcher(this.config.fetcher.apiKey, new Http_1.Http());
    };
    return WeatherFetcherProvider;
})();
exports.WeatherFetcherProvider = WeatherFetcherProvider;
