export interface WeatherConfig {
    fetcher: {
        implementation: string;
        apiKey: string;
    };
}
