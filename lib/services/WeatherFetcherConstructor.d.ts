import { WeatherFetcher } from "./WeatherFetcher";
import { Http } from "./Http";
export interface WeatherFetcherConstructor {
    new (apiKey: string, http: Http): WeatherFetcher;
}
