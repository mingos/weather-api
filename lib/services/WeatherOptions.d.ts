export interface WeatherOptions {
    city: string;
    country: string;
    countryCode: string;
    latitude: number;
    longitude: number;
}
