var extend = require("extend");
var OpenWeatherMapFetcher = (function () {
    function OpenWeatherMapFetcher(apiKey, http) {
        this.qs = {
            appid: apiKey,
            units: "metric"
        };
        this.apiUrl = "http://api.openweathermap.org/data/2.5/weather";
        this.http = http;
    }
    OpenWeatherMapFetcher.prototype.getWeather = function (options) {
        var _this = this;
        var requestOptions = {
            uri: this.apiUrl,
            qs: extend({ q: [options.city, options.country].join(",") }, this.qs)
        };
        return new Promise(function (fulfill, reject) {
            _this.http.get(requestOptions, function (error, response, body) {
                if (error) {
                    reject(error);
                }
                else {
                    var responseBody = JSON.parse(body);
                    fulfill({
                        temperature: responseBody.main.temp
                    });
                }
            });
        });
    };
    return OpenWeatherMapFetcher;
})();
exports.OpenWeatherMapFetcher = OpenWeatherMapFetcher;
