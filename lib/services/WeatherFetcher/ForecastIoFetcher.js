var ForecastIoFetcher = (function () {
    function ForecastIoFetcher(apiKey, http) {
        this.apiUrl = "https://api.forecast.io/forecast/" + apiKey;
        this.qs = {
            units: "si"
        };
        this.http = http;
    }
    ForecastIoFetcher.prototype.getWeather = function (options) {
        var _this = this;
        var latitude = encodeURIComponent(options.latitude.toString());
        var longitude = encodeURIComponent(options.longitude.toString());
        var requestOptions = {
            uri: this.apiUrl + "/" + latitude + "," + longitude,
            qs: this.qs
        };
        return new Promise(function (fulfill, reject) {
            _this.http.get(requestOptions, function (error, response, body) {
                if (error) {
                    reject(error);
                }
                else {
                    var responseBody = JSON.parse(body);
                    fulfill({
                        temperature: responseBody.currently.temperature
                    });
                }
            });
        });
    };
    return ForecastIoFetcher;
})();
exports.ForecastIoFetcher = ForecastIoFetcher;
