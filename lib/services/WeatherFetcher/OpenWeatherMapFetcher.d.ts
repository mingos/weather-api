import { WeatherFetcher } from "../WeatherFetcher";
import { WeatherOptions } from "../WeatherOptions";
import { Weather } from "../Weather";
import { Http } from "../Http";
export declare class OpenWeatherMapFetcher implements WeatherFetcher {
    private apiUrl;
    private qs;
    private http;
    constructor(apiKey: string, http: Http);
    getWeather(options: WeatherOptions): Promise<Weather>;
}
