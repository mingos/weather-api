import { Weather } from "./Weather";
import { WeatherOptions } from "./WeatherOptions";
export interface WeatherFetcher {
    getWeather(options: WeatherOptions): Promise<Weather>;
}
