import { WeatherInterface } from "./WeatherInterface";
export interface WeatherFetcherInterface {
    getWeather(city: string, country?: string): Promise<WeatherInterface>;
}
