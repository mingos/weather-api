import { WeatherFetcher } from "./WeatherFetcher";
import { WeatherConfig } from "./WeatherConfig";
export declare class WeatherFetcherProvider {
    private config;
    constructor(config: WeatherConfig);
    get(): WeatherFetcher;
}
