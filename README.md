# Weather API

A simple API for the weather forecast UI.

## Installation

All necessary dependencies can be installed using npm:

```
npm install
```

## Building and testing

To build the application, it is necessary to have **tsd** and **gulp** installed:

```
npm install -g tsd gulp
```

Then, install TypeScript typings and build the app:

```
npm run build
```

The last command will also run all the unit tests. In order to run them manually after the application is built, you
can use the command:

```
npm test
```

## Running the server

The server can be run using th command:

```
npm start
```

The server should start listening on port 3000 unless a different port is specified in the `PORT` env variable:

```
PORT=3001 npm sart
```
